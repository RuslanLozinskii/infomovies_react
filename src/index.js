import React from "react";
import ReactDOM from "react-dom";
import Routes from "./Components/Routes/Routes";
import "./style/index.css";
ReactDOM.render(
  <React.StrictMode>
    <Routes />
  </React.StrictMode>,
  document.getElementById("root")
);
