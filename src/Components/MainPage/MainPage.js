import React, { Component } from "react";
import Item from "./Item/Item";
import axios from "axios";
class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      image: [],
    };
    this.searchField = this.searchField.bind(this);
  }
  searchField(e) {
    let getMovies = this.state.movies;
    let result = getMovies.filter((value) => {
      return this._input.value.toLowerCase() === value.title.toLowerCase();
    });

    this.setState({
      movies: result,
    });
    this._input.value = "";
    e.preventDefault();
  }
  async componentDidMount() {
    axios
      .all([
        axios.get(
          "https://api.themoviedb.org/3/movie/popular?api_key=d55796f2ca4ecdb97ce23a56b9712c7f&language=en-US"
        ),
        axios.get(
          "https://api.themoviedb.org/3/configuration?api_key=d55796f2ca4ecdb97ce23a56b9712c7f&language=en-US"
        ),
      ])
      .then((res) => {
        this.setState({
          movies: res[0].data.results,
          image: res[1].data.images,
        });
      });
  }
  render() {
    return (
      <div className="container">
        <form id="search-header" onSubmit={this.searchField}>
          <input
            placeholder="find your movie"
            type="text"
            ref={(el) => {
              this._input = el;
            }}
          />
          <button type="submit">OK</button>
        </form>
        <div className="movies">
          <Item item={this.state.movies} image={this.state.image} />
        </div>
      </div>
    );
  }
}
export default MainPage;
