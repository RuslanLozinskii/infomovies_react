import { useHistory } from "react-router-dom";

function Header(props) {
  const history = useHistory();
  return (
    <header className="header">
      <div className="dropdown">
        <button
          id="header-home"
          onClick={() => {
            history.push("/InfoMovies_React/");
          }}
        >
          HOME
        </button>
      </div>
    </header>
  );
}
export default Header;
