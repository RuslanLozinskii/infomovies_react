import { useHistory } from "react-router-dom";
function Item(props) {
  const { item, image } = props;

  const history = useHistory();
  return item.map((value) => {
    return (
      <div className="movies-card" key={value.id}>
        <div className="movies-image">
          <img
            src={image.base_url + image.poster_sizes[4] + value.poster_path}
            alt=""
          />
        </div>
        <div className="movies-info">
          <h4>{value.title}</h4>
        </div>
        <p>rate:{value.popularity.toFixed()}</p>
        <button
          className="details"
          onClick={() => {
            history.push(`/InfoMovies_React/movies/${value.id}`, item);
          }}
        >
          details
        </button>
      </div>
    );
  });
}

export default Item;
