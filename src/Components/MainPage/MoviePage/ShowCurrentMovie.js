function ShowCurrentMovie(props) {
  let { data, image } = props;
  if (image.length !== 0) {
    return data.map((itemData) => {
      return (
        <div className="showItem" key={itemData.id}>
          <div className="showItem-image">
            <img
              alt=""
              src={
                image.base_url + image.poster_sizes[4] + itemData.poster_path
              }
            />
          </div>

          <div className="showItem-overview">
            <p>
              <b>overview:</b> {itemData.overview}
            </p>
          </div>
          <div className="showItem-info">
            <p>
              <b>data of release:</b> {itemData.release_date}
            </p>
            <p>
              <b>name:</b> {itemData.title}
            </p>
            <p>
              <b> country:</b> {itemData.production_companies[0].origin_country}
            </p>
            <p>
              <b>budget:</b> {itemData.budget}$
            </p>
            <p>
              <b>genre:</b> {itemData.genres[0].name}
            </p>
            <p>
              <b>language:</b> {itemData.spoken_languages[0].english_name}
            </p>
          </div>
        </div>
      );
    });
  } else {
    return null;
  }
}

export default ShowCurrentMovie;
