import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import ShowCurrentMovie from "./ShowCurrentMovie";

function CurrentMovie() {
  let params = useParams();
  let [dataMovie, setDataMovie] = useState([]);
  let [dataImage, setDataImage] = useState([]);
  useEffect(() => {
    if (params) {
      axios
        .all([
          axios.get(
            `https://api.themoviedb.org/3/movie/${params.movieId}?api_key=d55796f2ca4ecdb97ce23a56b9712c7f`
          ),
          axios.get(
            `https://api.themoviedb.org/3/configuration?api_key=d55796f2ca4ecdb97ce23a56b9712c7f&language=en-US`
          ),
        ])
        .then((res) => {
          setDataMovie([res[0].data]);
          setDataImage(res[1].data.images);
        });
    } else {
      console.error("something wrong");
    }
  }, [params]);

  return (
    <div className="itemInfo">
      <ShowCurrentMovie data={dataMovie} image={dataImage} />
    </div>
  );
}
export default CurrentMovie;
