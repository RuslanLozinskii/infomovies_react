import { Router, Switch, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import Layout from "./Layout";
import MainPage from "../MainPage/MainPage";
import CurrentMovie from "../MainPage/MoviePage/CurrentMovie";

function Routes() {
  const customHistory = createBrowserHistory();

  return (
    <Router history={customHistory}>
      <Switch>
        <Route exact path="/InfoMovies_React/">
          <Layout>
            <MainPage />
          </Layout>
              </Route>
        <Route exact path="/InfoMovies_React/movies/:movieId">
          <Layout>
            <CurrentMovie />
          </Layout>
        </Route>
      </Switch>
    </Router>
  );
}
export default Routes;
